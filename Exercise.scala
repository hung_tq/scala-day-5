import java.io.{BufferedReader, BufferedWriter, File, FileReader, FileWriter, PrintWriter}
import scala.io.Source

object Exercise {

  def replaceInFile(search: String, replace: String, fileName: String): Unit ={
    var text: String = readFile(fileName)
    createBackupFile(text,fileName)
    val update  = text.replaceAll(search,replace)
    writeFile(update,fileName)
  }

  def replaceMultiFiles(search: String, replace: String, files: List[String]): Unit ={

  }

  def readFile(fileName: String) = Source.fromFile(fileName).getLines().mkString("\n")

  def writeFile(s: String, fileName: String): Unit ={
   val writer = new FileWriter( new File(fileName))
    writer.write(s)
    writer.close()
  }

  def createBackupFile(s: String, fileName: String): Unit = {
    val dir = new File(fileName)

    var backupFile = new File(dir, s"${fileName}.bak")
    while (backupFile.exists()) {
      backupFile = new File(dir, s"${fileName}_${System.currentTimeMillis()}.bak")
    }
    writeFile(s, backupFile.getName)
  }

  def main(args: Array[String]): Unit = {
    replaceInFile("Hihi", "Hahahahahaahaha", "acb.txt")
}

}
